    /**
     * University of San Diego
     * COMP 285
     * Spring 2015
     * Instructor: Gautam Wilkins
     *
     * This class implements Huffman codes for files containing ASCII characters. It can read and compress a file and also
     * decode a compressed file and covert it back to its original ASCII characters.
     */

    import java.io.File;
    import java.io.FileReader;
    import java.io.IOException;
    import java.util.*;
    import java.util.PriorityQueue;


    public class HuffmanCode {

        /**
         * Opens a file containing ascii characters and returns them as an ArrayList of characters (e.g.
         * if the file contents were: "abc\nd ef" then the resulting ArrayList would be:
         * {'a', 'b', 'c', '\n', 'd', ' ', 'e', 'f'}
         */
        public static ArrayList<Character> charactersFromFile(String fileName) {
            ArrayList<Character> fileChars = new ArrayList<Character>();
            File f = new File(fileName);
            try {
                FileReader fr = new FileReader(f);
                int next;
                while ((next = fr.read()) != -1) {
                    Character current = (char) next;
                    fileChars.add(current);
                }

            } catch (IOException e) {
                System.out.println("Could not open file: " + fileName);
                System.exit(1);
            }
            return fileChars;
        }

        /**
         * Takes an array of characters and returns a HashMap where each key is a character in the array
         * the value is how many times that character appears in the array. (e.g. for {a, b, c, a, a, d, c}
         * the HashMap would be:
         * {a->3, b->1, c->2, d->1}
         *
         * For the frequencies method there’s a better way. You want to start by creating an empty HashMap.
         * Then, for each character in the ArrayList check to see if it’s present in the HashMap as a key.
         * If it’s not in the HashMap then add it along with the value 1 (since this character has appeared once so far).
         * If the character is in the HashMap as a key then look up the value, add one to the value, and store it back in the HashMap.

         To look up and store characters and their frequencies you would use code that looks like:

         HashMap<Character, Integer> map = new HashMap<Character, Integer>();

         //Insert the character “a” with frequency 1
         map.put(‘a', 1);

         //Check to see if the character “b” has a frequency.
         map.containsKey(‘b’);

         //Look up the frequency for the character “a” (the return value will be null if the frequency is 0)
         map.get(‘a’);
         */
        public static HashMap<Character, Integer> frequenciesFromArray(ArrayList<Character> charArray) {
            // Implement Me!
            int count = 0;
            HashMap<Character, Integer> map = new HashMap<Character, Integer>();
            if (charArray.isEmpty()) { //to check if the arraylist is empty or not
                System.out.print("Arraylist is Empty"); //prints if arraylist is empty
            }

            for (Character c : charArray) {

                if (map.containsKey(c)) {
                    map.put(c, map.get(c) + 1);
                } else {
                    map.put(c, 1);
                }
            }
            return map;
        }


            /**
             * Takes a HashMap with character frequencies and generates a HuffmanTree to optimally encode
             * the individual characters.
             */
        public static HuffmanTree generateCodingTree(HashMap<Character, Integer> charFreq) {
            // Implement Me!
            //
            // Hints:
            //
            // 1) Use the Java PriorityQueue class
            //
            // 2) To create a single Huffman Tree for a character (call it value) and a frequency, call:
            //      new HuffmanTree(char, frequency)
            //    You would use this at the setup stage when you create a Huffman tree with a single node
            //    for every character
            //
            // 3) To merge two Huffman trees (named left and right) call
            //    HuffmanTree parent = HuffmanTree.merge(left, right);
            //
            //    parent will have its frequency set to the sum of the frequencies of left and right, so
            //    you can immediately add it to the priority queue.
                /*
                  1. we would first create a new HuffmanTree,
                  2. then loop through the HashMap, and
                  3. putting the values and their respective frequencies inside a Priority Queue,
                  4. Choose the two values with the smallest frequencies and use the merge method in HuffmanTree.java to merge those two tries
                  5. Repeat with next smallest freq. and the HuffmanTree
                */
            PriorityQueue<HuffmanTree> queue = new PriorityQueue<HuffmanTree>();
            for(Map.Entry<Character, Integer> entry : charFreq.entrySet()) {
                Character c = entry.getKey();
                int cfreq = entry.getValue();
                HuffmanTree root = new HuffmanTree(c, cfreq);
                queue.add(root);
            }
            HuffmanTree finalT = null;
            while(!queue.isEmpty()) {
                HuffmanTree left = queue.remove();
                HuffmanTree right = queue.remove();
                HuffmanTree parent = HuffmanTree.merge(left, right);
                if(queue.isEmpty()){
                    finalT = parent;
                    break;
                }
                queue.add(parent);
            }
            return finalT;

        }




        /**
         * Takes an ArrayList of characters and a HashMap of character encodings and then
         * generates the String encoding for charArray (e.g. if
         * charArray = {a, b, c, c, a, a}
         * and codeMap = {a->{true}, b->{false, true, false}, c->{false, false}}
         * then the resulting string will be:
         * 1010000011, where
         *
         * 1 010 00 00 1 1
         * a  b  c  c  a a
         */
        public static String encodeCharacters(ArrayList<Character> charArray, HashMap<Character, ArrayList<Boolean>> codeMap) {
            String bitString = "";
            for (Character c : charArray) {
                String cstring = BinaryHelper.bitStringFromBooleanList(codeMap.get(c));
                bitString = bitString.concat(cstring);
            }
            return bitString;
        }


        /**
         * Helper method to print character encodings in a quickly readable format
         */
        public static void printEncoding(HashMap<Character, ArrayList<Boolean>> encoding) {
            for (Map.Entry<Character, ArrayList<Boolean>> entry : encoding.entrySet()) {
                System.out.println(entry.getKey() + ": " + BinaryHelper.bitStringFromBooleanList(entry.getValue()));
            }
        }












    }
